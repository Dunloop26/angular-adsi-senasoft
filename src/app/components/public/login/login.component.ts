import {
  Component,
  ComponentFactoryResolver,
  ElementRef,
  OnInit,
  AfterViewInit,
  Type,
  ViewChild,
} from '@angular/core';
import { ContainerHostDirective } from 'src/app/directives/public/container-host.directive';
import { SigninFormComponent } from '../signin-form/signin-form.component';
import { SignupFormComponent } from '../signup-form/signup-form.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, AfterViewInit {
  @ViewChild(ContainerHostDirective, { static: true })
  containerHost!: ContainerHostDirective;
  @ViewChild('btnContainer') btnContainer!: ElementRef;

  private buttons: Array<HTMLElement> = new Array<HTMLElement>();
  private activeIdx : number = -1;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

  ngOnInit(): void {
    this.onSignInClicked();
  }

  ngAfterViewInit(): void {
    const children = this.btnContainer.nativeElement.children;
    for (let child of children) {
      this.buttons.push(child);
    }

    // Defino el primer botón como presionado
    this.setActiveTab(0)
  }

  setActiveTab(idx : number) : void {
    // Si no hay botones activos
    if (this.buttons.length === 0) return;

    if (this.activeIdx == -1){
      // Desactivo todo la primera vez
      this.buttons.forEach(el => el.classList.remove('active'));
    } else {
      // Desactivo el botón anterior
      const previousButton = this.buttons[this.activeIdx];
      previousButton.classList.remove('active');
    }

    // Activo el botón seleccionado
    const button = this.buttons[idx];
    button.classList.add('active');

    // Defino el índice activo
    this.activeIdx = idx;
  }

  onSignInClicked() {
    this.createComponentRef(SigninFormComponent);
    this.setActiveTab(0);
  }

  onSignUpClicked() {
    this.createComponentRef(SignupFormComponent);
    this.setActiveTab(1);
  }

  createComponentRef<T>(component: Type<T>) {
    const componentFactory =
      this.componentFactoryResolver.resolveComponentFactory(component);
    const viewContainerRef = this.containerHost.viewContainerRef;

    viewContainerRef.clear();
    viewContainerRef.createComponent(componentFactory);
  }
}

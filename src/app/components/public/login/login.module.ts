import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SigninFormComponent } from '../signin-form/signin-form.component';

import { ContainerHostDirective } from 'src/app/directives/public/container-host.directive';
import { SignupFormComponent } from '../signup-form/signup-form.component';
import { GenericInputComponent } from '../../shared/generic-input/generic-input.component';

@NgModule({
  declarations: [
    LoginComponent,
    SigninFormComponent,
    SignupFormComponent,
    ContainerHostDirective,
    GenericInputComponent,
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    ReactiveFormsModule,
  ]
})
export class LoginModule { }

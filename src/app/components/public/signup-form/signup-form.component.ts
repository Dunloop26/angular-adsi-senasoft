import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss']
})
export class SignupFormComponent implements OnInit {

  constructor(private fb: FormBuilder) { }

  signUpForm : FormGroup = this.fb.group({
    user: ['', Validators.required],
    email: ['', [ Validators.email, Validators.required ]],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required]
  });

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.log(this.signUpForm.value)
  }

}

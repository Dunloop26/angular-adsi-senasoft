import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-generic-input',
  templateUrl: './generic-input.component.html',
  styleUrls: ['./generic-input.component.scss']
})
export class GenericInputComponent implements OnInit {

  constructor() { }

  @Input() startValue : any = undefined;
  @Input() inputType : string = 'text';
  @Input() labelText : string = 'Text';

  name! : FormControl

  ngOnInit() {
    this.name = new FormControl(this.startValue ?? '')
  }
}

import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[containerHost]'
})
export class ContainerHostDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
